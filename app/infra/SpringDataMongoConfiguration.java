package infra;


import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.PlatformTransactionManager;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

/**
 * Spring JavaConfig configuration class to setup a Spring container and
 * infrastructure components like a {@link DataSource}, a
 * {@link EntityManagerFactory} and a {@link PlatformTransactionManager}.
 */
@Configuration
@EnableMongoRepositories(basePackages="models")
public class SpringDataMongoConfiguration extends AbstractMongoConfiguration {

	/* 
	 * (non-Javadoc)
	 * @see org.springframework.data.mongodb.config.AbstractMongoConfiguration#getDatabaseName()
	 */
	@Bean
	@Override
	protected String getDatabaseName() {
		return "test";
	}

	/* 
	 * (non-Javadoc)
	 * @see org.springframework.data.mongodb.config.AbstractMongoConfiguration#mongo()
	 */
	@Bean
	@Override
	public Mongo mongo() throws Exception {
		Mongo mongo = new MongoClient("localhost");
		mongo.setWriteConcern(WriteConcern.SAFE);

		return mongo;
	}
}
