package controllers;

import models.CustomerRepository;
import infra.SpringContextUtils;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

public class Application extends Controller {

    public static Result index() {
    	CustomerRepository repository = SpringContextUtils.getSpringBean(CustomerRepository.class);
    	
//    	repository.save(new Customer("Alice", "Smith"));
//		repository.save(new Customer("Bob", "Smith"));
    	
        return ok(index.render("Your new application is ready." + repository.findAll()));
    }

}
