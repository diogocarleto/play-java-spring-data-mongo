name := """play-java-spring-data-mongo"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  cache,
  javaWs,
  javaCore,
  javaJpa,
  "org.springframework" % "spring-context" % "4.1.6.RELEASE",
  "org.springframework" % "spring-expression" % "4.1.6.RELEASE",
  "javax.inject" % "javax.inject" % "1",
  "org.springframework.data" % "spring-data-mongodb" % "1.6.0.RELEASE",
  "com.mysema.querydsl" % "querydsl-mongodb" % "2.9.0"
)
